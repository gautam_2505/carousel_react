import React, { useState } from 'react'
import { css, jsx } from '@emotion/core'
import SliderContent from './SliderContent'
import Slide from './Slide'

/**
 * @function Slider
 */
const Slider = (props) => {
  const getWidth = () => window.innerWidth

  const [state, setState] = useState({
    translate: 0,
    transition: 0.45
  })

  const { translate, transition } = state

  console.log('props=========>>>>>>', props);

  return (
    <div css={SliderCSS}>
      <SliderContent
        translate={translate}
        transition={transition}
        width={getWidth()}
      >
        {
          props.slides.map((slide, i) => <Slide key={i} content={slide}/>)
        }
        {/* <Slide /> */}
      </SliderContent>
    </div>
  )
}

const SliderCSS = css`
  position: relative;
  height: 100%;
  width: 100%;
  margin: 0 auto;
  overflow: hidden;
`
export default Slider






























// import React, { useState } from 'react';
// import SliderContent from './SliderContent';

// /**
//  * @function Slider
//  */
// const Slider = () => {
//   return <div style={{height: '650px', width: '100%', background: '#333'}}>
//     <SliderContent translate={450}/>
//   </div>
// }

// export default Slider